<?php
namespace app\Controllers;

class YoutubeParserController extends \core\Base{

	protected static $_instance;
	var $uses = array('YoutubeRequests');
	
	function get_data($tmp){
		$tmp = trim($tmp);
		$youtube_result_names = array();
		$youtube_result_url = array();
		$query=$this->YoutubeRequests->query("SELECT  
				`requests` . *,
				`result`.id AS `result_id`,
				`result`.name AS `result_name`,
				`result`.url AS `result_url`
			FROM  `youtube_requests` AS `requests` 
			LEFT JOIN  `youtube_results` AS  `result` ON ( requests.id = result.youtube_reguests_id ) 
			WHERE `requests`.`name` = '".mysql_real_escape_string(urldecode($tmp))."'
		");
		
		while($row = mysql_fetch_array($query,MYSQL_ASSOC))
		{
			$result[] = array(
				'YoutubeRequest'=>array(
					'id'   => $row['id'],
					'name' => $row['name'],
				),
				'YoutubeResults'=>array(
					'id'   => $row['result_id'],
					'youtube_reguests_id'   => $row['id'],
					'name' => $row['result_name'],
					'url' => $row['result_url']
				),
			);
		}

		if(!isset($result))
		{
			$this->YoutubeRequests->insert(array(
				'name'  => urldecode($tmp)
			));
			$youtube_reguests_id = $this->YoutubeRequests->lastInsertId();
		}
		else
		{
			$youtube_reguests_id  = $result[0]['YoutubeRequest']['id'];
			$youtube_result_names = \core\Set::extract($result,'{n}.YoutubeResults.name');
			$youtube_result_url   = \core\Set::extract($result,'{n}.YoutubeResults.url');
		}
		
		require_once ROOT .'/lib/simplehtmldom_1_5/simple_html_dom.php';
		
		$result = file_get_contents('https://www.youtube.com/results?q='.$tmp.'&sp=EgIQAQ%253D%253D');
		//$result = file_get_contents($this->YoutubeRequests->search_url.$tmp);
		
		$pattern = "#<div id=\"results\"(.*)role=\"navigation\">#Us";
		preg_match($pattern, $result, $matches);
		$matches[0] = preg_replace('/<!--(.*?)-->/', '', $matches[0]);
		$result = str_get_html('<html><body>'.$matches[0].'</body></html>');
		
		$number_check = 0; 
		$result_data = '';
		foreach($result->find('.item-section', 0)->find('.yt-lockup-video') as $element)
		{
			if(is_object($element))
			{
				if($number_check==10){
					break;
				}
				if(is_object($element->find('.yt-lockup-title',0)))
				{
					$number_check++;
					$title = trim($element->find('.yt-lockup-title',0)->find('a',0)->innertext());
					$url = 'https://www.youtube.com'.str_replace('https://www.youtube.com','',$element->find('.yt-lockup-title',0)->find('a',0)->href);
				}
				if(is_object($element->find('.yt-lockup-description',0)))
				{
					$description = trim($element->find('.yt-lockup-description',0)->innertext());
				}
				if(is_object($element->find('.yt-lockup-meta-info',0)))
				{
					if(is_object($element->find('.yt-lockup-meta-info',0)->find('li',1)))
					{
						$rating = $element->find('.yt-lockup-meta-info',0)->find('li',1)->innertext();
						$rating = preg_replace('~[^0-9]+~','',$rating);
					}
				}
				if(!empty($title) 
					and !in_array(mysql_real_escape_string($title), $youtube_result_names)
					and !in_array(mysql_real_escape_string($url), $youtube_result_url)
				)
				{
					$result_data .= "('', 
						'".$youtube_reguests_id."', 
						'".mysql_real_escape_string($title)."', 
						'".mysql_real_escape_string($rating)."',
						'".mysql_real_escape_string($description)."',
						'".mysql_real_escape_string($url)."'
					),";
				}
			}
		
		}
		$result->clear(); // подчищаем за собой
		unset($result);
		if($result_data)
		{
			$this->YoutubeRequests->query("insert into youtube_results 
				(
					id,
					youtube_reguests_id,
					name,
					video_rating,
					short_description,
					url
				)  
				values
					".trim($result_data,',')."
			");
			
			$this->YoutubeRequests->affectedRows();
		}
		exit;
	}


	function index($step=null){
		if($step=='get_data')
		{
			$youtube_requests = array();
			$data = array();
			if(isset($this->data['request']))
			{
				foreach($this->data['request'] as $v)
				{
					$v = urlencode(trim($v));
					$data[] = array('route'=>'YoutubeParser/get_data/'.$v);
					$youtube_requests[] = $this->YoutubeRequests->search_url.$v;
				}
				if(!empty($data))
				{
					$multithreading = new \core\MultiThreading();
					$multithreading->setParams($data);
					$multithreading->execute();
				}
			}
			header('Content-Type: application/json');
			echo json_encode($youtube_requests);
			exit;
		}
		
		if($step=='get_by_name')
		{
			$this->data['name'] = urldecode($this->data['name']);
			$this->data['name'] = str_replace($this->YoutubeRequests->search_url,'',$this->data['name']);
			$this->data['name'] = trim($this->data['name']);
			
			$query=$this->YoutubeRequests->query("
				SELECT  
					`requests` . *,
					`result`.id AS `result_id`,
					`result`.name AS `result_name`,
					`result`.video_rating AS `result_video_rating`,
					`result`.short_description AS `result_short_description`,
					`result`.url AS `result_url`
				FROM  `youtube_requests` AS `requests` 
				LEFT JOIN  `youtube_results` AS  `result` ON ( requests.id = result.youtube_reguests_id ) 
				WHERE `requests`.`name` = '".mysql_real_escape_string($this->data['name'])."'
			");
			$name = null;
			while($row = mysql_fetch_array($query,MYSQL_ASSOC))
			{
				if(!$name)
				{
					$name = $row['name'];
				}
				$result[] = array(
					'YoutubeRequest'=>array(
						'id'   => $row['id'],
						'name' => $row['name'],
					),
					'YoutubeResults'=>array(
						'id'   					=> $row['result_id'],
						'youtube_reguests_id'	=> $row['id'],
						'name' 					=> $row['result_name'],
						'video_rating' 			=> $row['result_video_rating'],
						'short_description' 	=> $row['result_short_description'],
						'url' 					=> $row['result_url']
					),
				);
			}
			
			//$youtube_results = \core\Set::combine($result,'{n}.YoutubeResults.id', '{n}');
			$youtube_results = count(\core\Set::extract($result,'{n}.YoutubeResults.id'));
			$average_rating  = array_sum(\core\Set::extract($result,'{n}.YoutubeResults.video_rating'))/$youtube_results;
			
			$result['general'] = array(
				'name'=>$name,
				'youtube_results'=>$youtube_results,
				'average_rating'=>$average_rating
			);
			header('Content-Type: application/json');
			echo json_encode($result);
			exit;
		}
		
		
		
		
		
	}
	
}
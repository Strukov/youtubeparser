<?php 

session_start();
header('Content-Type:text/html; charset=utf8');

error_reporting(E_ALL);

require_once(__DIR__ .'/basics.php');

use core\Router as Router;

$router = new Router();
$router->run();

?>

<?php
/**
 * Basic 
 *
 * Core functions for including other source files, loading models and so forth.
 *
 */
/**
 * Basic defines 
 */
	
	
/**
 *  Get Cake's root directory
 */
	define('APP_DIR', 'app');
	define('TEMPLATE', 'main');
	define('DS', DIRECTORY_SEPARATOR);
	define('ROOT', dirname(__FILE__));
	define('WEBROOT_DIR', 'webroot');
	define('WWW_ROOT', WEBROOT_DIR . DS . TEMPLATE . DS);
 
/**
 *
 * Functions autoload 
 * including class, loading models and so forth.
 *
 */
	spl_autoload_register(
		function ($class) {
			$filename = str_replace('\\', '/', $class). '.php';
			if (file_exists($filename)) {
				require_once $filename;
			}else{
				$cl = str_replace('\\', '/', $class);
				foreach(array('app/models','lib') as $k){
					$tmp = __DIR__ .'/'.$k.'/'.$cl.'.php';
					if(file_exists($tmp)){ 
						require_once $tmp; 
						break; 
					}
				}
			}
		}
	);
/**
 * Print_r convenience function, which prints out <PRE> tags around
 * the output of given array.
 *
 */
	if (!function_exists('pr')) {
		function pr($var){
			echo '<pre>';
			print_r($var);
			echo '</pre>';
		}
	}
	
	if (!function_exists('clean')){
		function clean($value = ""){
			$value = trim($value);
			$value = stripslashes($value);
			//$value = strip_tags($value);
			$value = htmlspecialchars($value);
			return $value;
		}
	}
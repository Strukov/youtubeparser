<?php
namespace core;

class Base
{

	public $data,$action,$controller,$view;
	private $member;  
	public $model = array();

	function __construct()
	{
		$this->data = $_REQUEST;		
		$this->view = new View();	
	}
	
    public static function getInstance($cl)
	{
		if (null === $cl::$_instance)
		{
			$cl::$_instance = new $cl();
        }
        return $cl::$_instance;
    }

	function redirect($url)
    {
        if($url[0]!='/')
        {
            $url[0]='/';
        }
        header('Location: '.$url, true, 303);
        exit();
    }
	
	function __set($name,$val)
	{
		$this->member[$name] = $val;
	}

	function __get($name)
	{
		if(in_array($name,$this->uses)){
			eval('$this->model["'.$name.'"] = new app\\Models\\'.$name.'();');
			return $this->model[$name];
		}else{
			return $this->member;
		}
	}
}


<?php
namespace core;

class MultiThreading
{
    /**
     * ��� �������
     *
     * @var string
     * @access private
     */
    public $server;
    
    /**
     * ������������ ���������� �������
     *
     * @var int
     * @access private
     */
    private $maxthreads;
    
    /**
     * ��� �������, ������� ��������� ������ ��� ������
     *
     * @var string
     * @access private
     */
    private $scriptname;
    
    /**
     * ���������, ������� �� ����� ���������� �������
     *
     * @var array
     * @access private
     */
    private $params = array();
    
    /**
     * ������, � ������� �������� ������
     *
     * @var array
     * @access private
     */
    private $threads = array();
    
    /**
     * ������, � ������� �������� ����������
     *
     * @var array
     * @access private
     */
    private $results = array();
    
    /**
     * ����������� ������. � ��� �� ��������� ������������ ���������� ������� � ��� �������. ��� ��������� �������������.
     *
     * @param int $maxthreads ������������ ���������� �������, �� ��������� 10
     * @param string $server ��� �������, �� ��������� ��� �������, �� ������� �������� ����������
     * @access public
     */
    public function __construct($maxthreads = 10, $server = '')
    {
        if ($server)
            $this->server = $server;
        else
            $this->server = $_SERVER['SERVER_NAME'];
        
        $this->maxthreads = $maxthreads;
    }
    
    /**
     * ��������� ��� �������, ������� ��������� ������ ��� ������
     *
     * @param string $scriptname ��� �������, ������� ���� � ����
     * @access public
     */
    public function setScriptName($scriptname)
    {
        if (!$fp = fopen('http://'.$this->server.'/'.$scriptname, 'r'))
            throw new Exception('Cant open script file');
        
        fclose($fp);
        
        $this->scriptname = $scriptname;
    }
    
    /**
     * ������ ���������, ������� �� ����� ���������� �������
     *
     * @param array $params ������ ����������
     * @access public
     */
    public function setParams($params = array())
    {
        $this->params = $params;
    }
    
    /**
     * ��������� ������, ����������� � ����
     *
     * @access public
     */
    public function execute()
    {
        // ��������� ��������, � �� ��������, ���� �� ���������� ��� ������
        do {
            // ���� �� ��������� ����� �������
            if (count($this->threads) < $this->maxthreads) {
                // ���� ������� �������� ��������� ����� ����������
                if ($item = current($this->params)) {
                
                    // ��������� ������ ������� GET
                    
                    $query_string = '';
                
                    foreach ($item as $key=>$value)
                        $query_string .= '&'.urlencode($key).'='.urlencode($value);
                    
                    $query = "GET http://".$this->server."/".$this->scriptname."?".$query_string." HTTP/1.0\r\n";
                    
                    // ���������� ����������
                    
                    if (!$fsock = fsockopen($this->server, 80))
                        throw new Exception('Cant open socket connection');
                
                    fputs($fsock, $query);
                    fputs($fsock, "Host: $this->server\r\n");
                    fputs($fsock, "\r\n");
                
                    stream_set_blocking($fsock, 0);
                    stream_set_timeout($fsock, 3600);
                    
                    // ���������� �����
                
                    $this->threads[] = $fsock;
                    
                    // ��������� � ���������� ��������
                
                    next($this->params);
                }
            }
            
            // ���������� ������
            foreach ($this->threads as $key=>$value) {
                // ���� ����� ���������, ��������� � �������
                if (feof($value)) {
                    fclose($value);
                    unset($this->threads[$key]);
                } else {
                    // ����� ��������� ����������
					$tmp = fgets($value);
					if($tmp){
						 $this->results[] = $tmp;
					}
                   
                }
            }
            
            // ����� ��������� ��������, ����� �� �������� ������
            //sleep(1);
            
        // ... ���� �� ���������� ��� ������    
        } while (count($this->threads) > 0);
    
        return $this->results;
    }
}
<?php
namespace core;

class DbController
{
 
    public function getAdapter()
    {
        $adapter    = ConfigHandler::getInstance()->getDb();
        $class      = 'Db'.$adapter.'Adapter';
        return call_user_func(array(
            $class,
            'getInstance'
        ));
    }
 
    public function getCredentials()
    {
        return ConfigHandler::getInstance()->getCredentials();
    }
}
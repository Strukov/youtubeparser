<?php
namespace core;

class Router extends Base
{

	function __construct()
	{
		$this->controller = 'YoutubeParser';		
		$this->action = 'index';
	}

	function run()
	{
		$routes = explode('/', $_GET['route']);
		if(!empty($routes[0])){ 
			$this->controller = $routes[0];  
		}
		if(!empty($routes[1])){ 
			$this->action = $routes[1];  
		}
		array_shift($routes);
		array_shift($routes);
		$controller = $this->getInstance("\\app\\Controllers\\".$this->controller."Controller");
		$controller->controller = $this->controller;
		$controller->action = $this->action;
		foreach($routes as $k => $v ){ 
			$routes[$k] = "'".$v."'"; 
		}
		eval('$controller->'.$this->action.'('.implode(',',$routes).');'); 
		if(empty($controller->view->logout))
		{
			$controller->view->logout = $controller->controller.'/'.$controller->action; 
		}
		$member['content'] = $controller->view->run( APP_DIR . DS .'views'. DS .$controller->view->logout.'.phtml', $controller->member);
		echo $controller->view->run(WWW_ROOT . $controller->view->template.'.phtml',$member);
	}
}

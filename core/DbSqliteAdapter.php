<?php
namespace core;

class DbSqliteAdapter extends DbAdapter
{
    protected $lastError;
 
    public static function getInstance()
    {
        if(!parent::$rInstance)
        {
            parent::$rInstance = new DbSqliteAdapter();
        }
        return parent::$rInstance;
    }
 
    function __construct()
    {
        if(!$this->_connect())
        {
            return false;
        }
    }
	
	function find($key, $tmp = array())
    {
		$fields=array('*');
		$conditions=null;
		$group=null;
		$order=null;
		$having=null;
		$limit=null;
		if($tmp)
		{
			extract($tmp);
		}
        if(!parent::$rLink)
        {
            return false;
        }
        $sql=$this->_get_select_sql($this->name, $fields, $conditions, $group, $order, $having, $limit);
	    if(!$sql)
        {
            return false;
        }
		$rgResult=false;
        $result    = mysql_query($sql,parent::$rLink);
		while($tmp = mysql_fetch_array($result,MYSQL_ASSOC))
		{
			$rgResult[] = $tmp;
		}
        if(!$rgResult)
        {
            $rgResult=false;
        }
        return $rgResult;
    }
 
    function select(
            $rgCols=array('*'),
            $rgWhere=null,
            $rgGroup=null,
            $rgOrder=null,
            $rgHaving=null,
            $rgLimit=null)
    {
        if(!parent::$rLink)
        {
            return false;
        }
        $sql=$this->_get_select_sql($this->name, $rgCols, $rgWhere, $rgGroup, $rgOrder, $rgHaving, $rgLimit);
        if(!$sql)
        {
            return false;
        }
        $result    = mysql_query($sql,parent::$rLink);
		while($tmp = mysql_fetch_array($result,MYSQL_ASSOC)){
			$rgResult[] = $tmp;
		}
        if(!$rgResult)
        {
            $rgResult=false;
        }
        return $rgResult;
    }
 
    public function replace()
    {
 
    }
 
    public function update($rgFields, $rgWhere=null)
    {
        if(!is_array($rgFields)||!count($rgFields))
        {
            return false;
        }
        $sql=$this->_get_update_sql($this->name, $rgFields, $rgWhere);
        $bResult    = $this->query($sql);
        return $bResult;
    }
 
    public function insert($rgFields, $sPrimaryField='id')
    {
        if(!array_key_exists($sPrimaryField, $rgFields))
        {
            $rgFields[$sPrimaryField]=$this->_get_next_id($sPrimaryField);
        }
        $sql=$this->_get_insert_sql($this->name, $rgFields);
        $bResult    = $this->query($sql);
        return $bResult;
    }
 
    public function delete($rgWhere)
    {
        $sql=$this->_get_delete_sql($this->name, $rgWhere);
        $bResult    = $this->query($sql);
        return $bResult;
    }
 
    public function query($sql)
    {
        return mysql_query($sql, parent::$rLink);
    }
 
    protected function _get_next_id($sPrimaryField='id')
    {
        $rgMax=$this->select(array('MAX('.$sPrimaryField.')'));		
        return current(current($rgMax))+1;
    }
 
    protected function _connect()
    {
        $rDb            = new DbController();
        $rgCredentials  = $rDb->getCredentials();
        if(!parent::$rLink)
        {
            try
            {
				parent::$rLink=mysql_connect($rgCredentials['host'], $rgCredentials['user'], $rgCredentials['password']);
				mysql_select_db($rgCredentials['database'], parent::$rLink);
				mysql_query('SET NAMES "utf8"', parent::$rLink);
				if(!parent::$rLink)
                {
                    $this->lastError=$sLastError;
                    return false;
                }
            }
            catch(Exception $e)
            {
                return false;
            }
        }
        return true;
    }
 
    protected function  _escape_string($string)
    {
        return mysql_real_escape_string($string);
    }
 
}

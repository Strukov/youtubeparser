<?php
namespace core;

class View
{

	public $template = 'index';
    public $logout = '';
	
	function run($dir, $member = null){	
		if($member)
		{
			extract($member);
		}
		ob_start(); 
		include ($dir);
		$layout = ob_get_contents();
		ob_end_clean(); 
		return $layout;
	}


}
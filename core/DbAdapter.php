<?php
namespace core;

abstract class DbAdapter
{
    static protected $rInstance = null;
    static protected $rLink     = null;
 
    protected $lastInsertId;
    protected $iAffectedRows;
 
    abstract public static function getInstance();
    abstract public function select(
            $rgCols=array('*'),
            $rgWhere=null,
            $rgGroup=null,
            $rgOrder=null,
            $rgHaving=null,
            $rgLimit=null);
	abstract public function find(
            $key, $tmp = array());
    abstract public function update($rgFields, $rgWhere=null);
    abstract public function replace();
    abstract public function insert($rgFields, $sPrimaryField=null);
    abstract public function delete($rgWhere);
    abstract public function query($sql);
    abstract protected function _escape_string($string);
 
    public function lastInsertId()
    {
        return mysql_insert_id(self::$rLink);
    }
 
    public function affectedRows()
    {
        return mysql_affected_rows(self::$rLink);
    }
 
    protected function _generic_escape($string)
    {
        return addslashes($string);
    }
 
    protected function _get_delete_sql(
            $table,
            $rgWhere
            )
    {
        if(!$table)
        {
            return false;
        }
        $rgDelete=array('DELETE FROM');
        $rgDelete[]=$table;
        if($rgWhere)
        {
            $rgDelete[]=$this->_get_sql_where($rgWhere);
        }
        return join(' ', $rgDelete);
    }
 
    protected function _get_insert_sql(
            $table,
            $rgFields
            )
    {
        if(!$table || !is_array($rgFields) || !count($rgFields))
        {
            return false;
        }
        $rgTableKeys=array();
        $rgTableValues=array();
        foreach(array_keys($rgFields) as $field)
        {
            $rgTableKeys[]=$field;
        }
        foreach(array_values($rgFields) as $value)
        {
            $rgTableValues[]="'".$this->_escape_string($value)."'";
        }
        $rgInsert=array('INSERT INTO');
        $rgInsert[]=$table;
        $rgInsert[]='('.join(',', $rgTableKeys).')';
        $rgInsert[]='VALUES';
        $rgInsert[]='('.join(',', $rgTableValues).')';
        return join(' ', $rgInsert);
    }
 
    protected function _get_update_sql(
            $table,
            $rgFields,
            $rgWhere
            )
    {
        if(!$table || !is_array($rgFields) || !count($rgFields))
        {
            return false;
        }
 
        $rgUpdate=array('UPDATE');
        $rgUpdate[]=$table;
        $rgUpdate[]='SET';
        $rgUpdateStr=array();
        foreach($rgFields as $field=>$value)
        {
            $rgUpdateStr[]=$field.'='."'".$this->_escape_string($value)."'";
        }
        $rgUpdate[]=join(',', $rgUpdateStr);
        if($rgWhere)
        {
            $rgUpdate[]=$this->_get_sql_where($rgWhere);
        }
        return join(' ', $rgUpdate);
    }
 
    protected function _get_select_sql(
            $rgFrom,
            $rgCols=array('*'),
            $rgWhere=null,
            $rgGroup=null,
            $rgOrder=null,
            $rgHaving=null,
            $rgLimit=null)
    {
        if(!$rgFrom)
        {
            return false;
        }
        $rgSelect=array('SELECT');
        $rgSelect[]=is_array($rgCols)?join(',', $rgCols):$rgCols;
        $rgSelect[]='FROM';
        if(!is_array($rgFrom))
        {
            $rgSelect[]=$rgFrom;
        }
        if($rgWhere)
        {
            $rgSelect[]=$this->_get_sql_where($rgWhere);
        }
 
        //the rest is TODO
        return join(' ', $rgSelect);
    }
 
    protected function _get_sql_where($rgWhere)
    {
        $strWhere=array();
        if($rgWhere)
        {
            $strWhere[]='WHERE';
            $rgWhereStr=array();
            if(is_array($rgWhere))
            {
                foreach($rgWhere as $field=>$condition)
                {
                    if(is_array($condition))
                    {
                        //TODO
                    }
                    else
                    {
                        $rgWhereStr[]='('.$field."='".$this->_escape_string($condition)."')";
                    }
                }
                $strWhere[]=join(' AND ', $rgWhereStr);
            }
            else
            {
                $strWhere[]=$rgWhere;
            }
        }
        return join(' ', $strWhere);
    }
}
 